#!/usr/bin/awk -f

BEGIN {
	sizeset = 0
	colorset = 0
	layers = 0
	destinationfile = "/dev/stdout"
	i = 1
	includes = 0
	while( i < ARGC && substr(ARGV[i],1,1) == "-" ) {
		if( ARGV[i] == "-o" ) {
			ARGV[i] = ""
			i++;
			destinationfile = ARGV[i]
			ARGV[i] = ""
			i++;
		} else if( ARGV[i] == "-I" ) {
			ARGV[i] = ""
			i++;
			INCLUDES[includes] = ARGV[i]
			includes++
			ARGV[i] = ""
			i++;
		} else if( ARGV[i] == "-c" ) {
			ARGV[i] = ""
			i++;
			if( ARGV[i] == "Yellow" ) {
				colorset = 1
				red = 1
				green = 1
				blue = 0
			} else if( ARGV[i] == "Blue" ) {
				colorset = 1
				red = 0
				green = 0
				blue = 1
			} else if( ARGV[i] == "Green" ) {
				colorset = 1
				red = 0
				green = 1
				blue = 0
			} else if( ARGV[i] == "Grey" ) {
				colorset = 1
				red = 0.7
				green = 0.7
				blue = 0.7
			} else {
				print "Unknown color " ARGV[i] "!" >> "/dev/stderr"
				exit 1
			}
			ARGV[i] = ""
			i++;
		} else {
			print "Unknown option '" ARGV[i] "'" >> "/dev/stderr"
			exit 1
		}
	}
	_ord_["0"] = 0; _ord_["1"] = 1; _ord_["2"] = 2; _ord_["3"] = 3
	_ord_["4"] = 4; _ord_["5"] = 5; _ord_["6"] = 6; _ord_["7"] = 7
	_ord_["8"] = 8; _ord_["9"] = 9; _ord_["a"] = 10; _ord_["b"] = 11
	_ord_["c"] = 12; _ord_["d"] = 13; _ord_["e"] = 14; _ord_["f"] = 15
	_ord_["A"] = 10; _ord_["B"] = 11
	_ord_["C"] = 12; _ord_["D"] = 13; _ord_["E"] = 14; _ord_["F"] = 15
}

function hextoval(s) {
	return _ord_[substr(s, 1, 1)] * 16 + _ord_[substr(s, 2, 1)]
}

function traverse_color(color, red, green, blue, 	val,r,g,b ) {
#	print color ":" red ":" green ":" blue
	if( match(color, /#([0-9a-fA-F][0-9a-fA-F])([0-9a-fA-F][0-9a-fA-F])([0-9a-fA-F][0-9a-fA-F])$/, val) > 0 ) {
		r = hextoval(val[1])
		g = hextoval(val[2])
		b = hextoval(val[3])
		color = sprintf("#%02x%02x%02x", red * r, green * g, blue * b)
#		print color
	} else {
		print "Cannot translate " color >> "/dev/stderr"
	}
	return color
}
function fullcolorchar(colorchar, colorlen) {
	while( length(colorchar) < colorlen )
		colorchar = " " colorchar
	return colorchar
}
function fullcolorline(line, colorlen, image_width, special,  	j, cc, nc, fullline) {
	fullline = ""
	for( j = 1 ; j <= length(line) ; j++ ) {
		cc = substr(line, j, 1)
		nc = cc
		while( cc in special && j < length(line) ) {
			j++
			cc = substr(line, j, 1)
			nc = nc cc
		}
#		print "char is '" nc "' escaped to '" fullcolorchar(nc, colorlen) "'"
		fullline = fullline fullcolorchar(nc, colorlen)
#		print "length is now " length(fullline)
	}
	if( length(fullline) != colorlen * image_width ) {
		print "Something went wrong! (" length(fullline) " != " colorlen " * " image_width ")!" > "/dev/stderr"
		print "'" fullline "'" > "/dev/stderr"
		exit 1
	}
#	print "Fulline is " fullline
	return fullline
}

/^file / {
        shortfilename = $2
	shortfilenames[fileofs] = shortfilename
	for( i = 0 ; i < includes ; i++ ) {
		filename = INCLUDES[i] "/" shortfilename
		if( (getline < filename) >= 0) {
			break
		}
	}
	if( i >= includes ) {
		print "Could not find " shortfilename >> "/dev/stderr"
		exit 1
	}
	filenames[fileofs] = filename
	fileofs ++
        if( $0 != "/* XPM */" ) {
                print filename " misses XPM header!" >> "/dev/stderr"
                exit 1
        }
        getline < filename
        if( match($0,/^static +char *\* *([a-zA-Z_]+) *\[ *\] *= *{ *$/, val) == 0 ) {
                print filename " has strange 2nd line!" >> "/dev/stderr"
                exit 1
        }
        xpmname = val[1]
#        print "name is " xpmname
        getline < filename
        if( match($0,/^" *([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+)", *$/, val) == 0 ) {
                print filename " has strange 3nd line!" >> "/dev/stderr"
                exit 1
        }
        width= 0 + val[1]
	widths[fileofs] = width
        height= 0 + val[2]
	heights[fileofs] = height
        numcolors= 0 + val[3]
	numscolors[fileofs] = numcolors
        if( val[4] != 1 ) {
                print filename " has too many bytes per pixel!" >> "/dev/stderr"
                exit 1
        }
        for( i = 0 ; i < numcolors ; i++ ) {
                getline < filename
                if( match($0,/^"(.)[\t ]+c +(#[0-9a-fA-F]+|[A-Z][a-z]+)", *$/, val) == 0 ) {
                        print filename " has strange " i "th color!" >> "/dev/stderr"
                        exit 1
                }
                if( val[2] == "None" ) {
                        nonecolor[fileofs] = val[1]
		}
                colors[fileofs, val[1]] = val[2]
        }
        for( y = 0 ; y < height-1 ; y++ ) {
                getline < filename
#		print y " " $0
                if( match($0,/^"([^"]*)" *, *$/, val) == 0 ) {
			print filename " has strange data line " y ":" >> "/dev/stderr"
			print >> "/dev/stderr"
			exit 1
                }
		if( length(val[1]) != width ) {
			print filename " has data line " y " of wrong length:" >> "/dev/stderr"
			print >> "/dev/stderr"
			exit 1
		}
		data[fileofs, y] = val[1]
	}
	y = height - 1
	getline < filename
#	print y " " $0
	if( match($0,/^"([^"]*)" *\} *; *$/, val) == 0 ) {
		print filename " has strange last data line " y ":" >> "/dev/stderr"
		print >> "/dev/stderr"
		exit 1
        }
	if( length(val[1]) != width ) {
		print filename " has data line " y " of wrong length:" >> "/dev/stderr"
		print >> "/dev/stderr"
		exit 1
	}
	data[fileofs, y] = val[1]
	close(filename)
#	print "reading " filename " finished successfully"
        next
}
/^size / {
	sizeset = 1
	image_width = 0 + $2
	image_height = 0 + $3
	if( image_width == 0 ) {
		print "missing or zero width in " FILENAME >> "/dev/stderr"
		exit 1
	}
	if( image_height == 0 ) {
		print "missing or zero height in " FILENAME >> "/dev/stderr"
		exit 1
	}
	next
}
/^use / {
	layer_image[layers] = $2
	ofs = 0 + $2
	layer_x[layers] = 0
	layer_y[layers] = 0
	layer_w[layers] = 0 + widths[ofs]
	layer_h[layers] = 0 + heights[ofs]
	layer_ofsx[layers] = 0
	layer_ofsy[layers] = 0
	for( i = 3 ; i <= NF ; i++ ) {
		if( match($i, /^([0-9]+)x([0-9]+)$/, val) > 0 ) {
			layer_w[layers] = 0 + val[1]
			layer_h[layers] = 0 + val[2]
		} else if( match($i, /^([0-9]+),([0-9]+)$/, val) > 0 ) {
			layer_ofsx[layers] = 0 + val[1]
			layer_ofsy[layers] = 0 + val[2]
		} else if( match($i, /^([-+][0-9]+)([-+][0-9]+)$/, val) > 0 ) {
			layer_x[layers] = layer_x[layers] + val[1]
			layer_y[layers] = layer_y[layers] + val[2]
		} else if( $i == "color" ) {
			layer_colormodifier[layers] = 1
			if( !colorset ) {
				print FILENAME ":" NR ": must call with -c to get color!" >> "/dev/stderr"
				exit 1
			}
			layer_red[layers] = 0 + red
			layer_green[layers] = 0 + green
			layer_blue[layers] = 0 + blue
		} else if( match($i, /^([0-9.]+)\/([0-9.]+)\/([0-9.]+)$/, val) > 0 ) {
			layer_colormodifier[layers] = 1
			layer_red[layers] = 0 + val[1]
			layer_green[layers] = 0 + val[2]
			layer_blue[layers] = 0 + val[3]
		} else {
			print FILENAME ":" NR ": cannot understand '" $i "'!" >> "/dev/stderr"
			exit 1
		}
#		print $i
	}
	layers++
	next
}
/^write$/ {
	allchars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz%!_#*^?@=~/&$|0123456789'`^"
	extrachars = allchars
	curprefix = ""
	specialchars = ".,:;+-<>[]{}()"
	for( i = 1 ; i <= length(specialchars) ; i++ ) {
		special[substr(specialchars, i, 1)] = 1
	}
	colorlen = 1
	usednone = 0
	if( sizeset == 0 ) {
		print "missing size before write in " FILENAME >> "/dev/stderr"
		exit 1
	}
	if( layers == 0 ) {
		print "Warning no output specified!" >> "/dev/stderr"
	}
	numcolors = 0
	newcolors[" "] = "None"
	for( y = 0 ; y < image_height ; y++ ) {
		final[y] = ""
		for( x = 0 ; x < image_width ; x++ ) {
			needed = 1
			for( l = 0 ; needed && l < layers ; l++ ) {
				if( layer_x[l] > x )
					continue;
				xim = x - layer_x[l]
				if( xim >= layer_w[l] )
					continue;
				xim = xim + layer_ofsx[l]
				if( layer_y[l] > y )
					continue;
				yim = y - layer_y[l]
				if( yim >= layer_h[l] )
					continue;
				yim = yim + layer_ofsy[l]
				im = layer_image[l]
				colorchar = substr(data[im, yim], 1+xim, 1)
				color = colors[im, colorchar]
				if( color == "None" )
					continue
				if( l in layer_colormodifier ) {
					color = traverse_color(color, layer_red[l], layer_green[l], layer_blue[l])
				}
				if( color in fincolors ) {
					colorchar = fincolors[color]
				} else {
					while( 1 ) {
						while( (colorchar in newcolors || colorchar in special) &&
							length(extrachars) > 0 ) {
							colorchar = curprefix substr(extrachars, 1, 1)
							extrachars = substr(extrachars, 2)
						}
						if( colorchar in newcolors || colorchar in special ) {
							if( colorlen == 1 ) {
								colorlen = 2
								extrachars = allchars
								curprefix = substr(specialchars, 1, 1)
								specialchars = substr(specialchars, 2)
								continue
							} else if( length(specialchars) > 0 ) {
								extrachars = allchars
								curprefix = substr(specialchars, 1, 1)
								specialchars = substr(specialchars, 2)
								continue
							} else {
								print "Running out of colors after " numcolors " colors!"
								exit 1
							}
						}
						break
					}
#					print "Setting color " colorchar " to " color
					fincolors[color] = colorchar
					newchars[numcolors] = colorchar
					newcolors[colorchar] = color
					numcolors++
				}
				final[y] = final[y] colorchar
				needed = 0
				break
			}
			if( needed ) {
				usednone = 1
				final[y] = final[y] " "
			}
		}
	}
	print "/* XPM */" > destinationfile
	print "static char *generated []={" >> destinationfile
	if( usednone ) {
		numcolors ++
	}
	print "\"" image_width " " image_height " " numcolors " " colorlen "\"," >> destinationfile
	if( usednone ) {
		print "\"" fullcolorchar(" ",colorlen) "\tc None\"," >> destinationfile
		numcolors --
	}
	for( i = 0 ; i < numcolors ; i++ ) {
		c = newchars[i]
#		print i ":" c ":" newcolors[c]
		print "\"" fullcolorchar(c, colorlen) "\tc " newcolors[c] "\"," >> destinationfile
	}
	for( y = 0 ; y < image_height - 1 ; y++ ) {
		fulline = fullcolorline(final[y], colorlen, image_width, special)
		print "\"" fulline "\"," >> destinationfile
	}
	y = image_height - 1;
	fulline = fullcolorline(final[y], colorlen, image_width, special)
	print "\"" fulline "\"};" >> destinationfile
	close(destinationfile);
	exit 0
	next
}
/^#/ { next }
/^ *$/ { next }
	{
		print "unknown command in " FILENAME ":" >> "/dev/stderr"
		print >> "/dev/stderr"
		exit 1
	}
