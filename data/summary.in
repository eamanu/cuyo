#
#   Copyright 2006 by Immanuel Halupczok
#   Modified 2006,2008,2011 by Immanuel Halupczok
#   Modified 2006,2008,2010,2011 by Mark Weyer
#   Maintenance modifications 2006,2008,2010-2012 by the cuyo developers
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

# This file is read by genSummary.pl to generate summary.ld
# Commands:
# - file xxx.ld # reads information out of level file xxx.ld
# - verbatim
#     xxx
#   end
#   # puts a verbatim copy of xxx into summary.ld
#   # even including blank lines and comments

verbatim
ignorelevel = Bunt,Dungeon,ReversiBRL,Slime,Zahn

level[all]=
  # easy
  Nasenkugeln,	# First level at all
  Schemen,	# Introduces that levels are rather different
  Embroidery,
  Maennchen,	# First level with weight<>1
  Octopi,	# Introduces neighbours_eight
  Springer,	# Introduces neighbours_knight and (somehow) single player greys
  Kacheln_Sechseck,	     # First level with hex mode and neighbours_hex6
  # medium
  Tiere,	# Introduces chaingrass=1
  Baender,	# Introduces neighbours_diagonal
  HexKugeln,	# First level with randomgreys
  Bonus,Pfeile,Fische,Theater,Pressure,Rechnen,
  Jahreszeiten,Disco,Wohnungen,Baggis,Tetris,
  Kacheln_Viereck,
  Rollenspiel,  # First level with different numexplodes
  Himmel,	# Introduces mirror=1
  Kolben,Baelle,Aliens,
  Labyrinth,Unmoeglich,Elemente,Slime,Hormone,Flechtwerk,
  ParatroopersInvers,
  # hard
  Explosive,Puzzle,Letters,SilberGold,Gold,DreiD,Zahn,Kunst,Augen,Doors,Bunt,
  Antarctic,Rohre,Darken,Kacheln_Fuenfeck,Ebene,Dungeon,
  ColorShape,Jump,Viecher,Memory,Schach,Aehnlich,ReversiBRL,
  Go,Tennis,
  # very hard
  Angst,Labskaus2,Walls,Kacheln_azyklisch,Trees2,Wuerfel,NoVIPs,Wachsen,GoII,
  mfs,BoniMali2

level[main]=
  Nasenkugeln,Schemen,Embroidery,Maennchen,Octopi,Springer,Kacheln_Sechseck,
  Tiere,Baender,HexKugeln,Bonus,Pfeile,Fische,Theater,Pressure,
  Jahreszeiten,Disco,Wohnungen,Baggis,
  Kacheln_Viereck,Rollenspiel,Himmel,Kolben,Baelle,
  Labyrinth,Elemente,Hormone,
  Explosive,Puzzle,Letters,SilberGold,DreiD,Zahn,Kunst,Augen,Doors,Bunt,
  Antarctic,Darken,Kacheln_Fuenfeck,Ebene,Dungeon,
  ColorShape,Viecher,Memory,
  Tennis,
  Angst,Labskaus2,Walls,Wuerfel,Wachsen,BoniMali2

level[main,easy]=
  Nasenkugeln,Schemen,Embroidery,Maennchen,Kacheln_Sechseck,
  Bonus,Pfeile,Fische,Theater,
  Kacheln_Viereck,Kolben,
  Labyrinth,Unmoeglich,
  Puzzle,SilberGold,DreiD,Bunt,
  Kacheln_Fuenfeck,Dungeon,
  Aehnlich,
  Tennis,
  Angst,Kacheln_azyklisch,Trees2,Wuerfel,BoniMali2

level[main,hard]=
  Maennchen,Springer,
  Fische,Theater,Rechnen,
  Aliens,
  Slime,
  Explosive,Puzzle,Gold,
  Darken,Dungeon,
  Memory,Aehnlich,
  Tennis,
  Angst,Labskaus2,Walls,Kacheln_azyklisch,Trees2,NoVIPs,Wachsen,GoII,
  BoniMali2

level[game]=
  Springer,ParatroopersInvers,
  Tetris,
  Rollenspiel,Aliens,
  Puzzle,Letters,
  Dungeon,
  Jump,Memory,Schach,ReversiBRL,
  Tennis,
  GoII,
  BoniMali2

level[extreme]=
  Bonus,		# 1+2 Sorten, numexplode=1, toptime=4
  Theater,		# 84 Stueck Gras
  Slime,		# 1+1 Sorten
  Explosive,		# 1+2 Sorten
  Puzzle,		# 1+0 Sorten
  Gold,			# 1+0 Sorten
  Zahn,			# 1+0/1 Sorten (1+0 oder 1+1, wie man's nimmt)
  Bunt,			# 136 Sorten, numexplode=2
  Rohre,		# 2+2 Sorten, numexplode=20/18
  BoniMali2,		# 14 Sorten
  Memory,		# 13 Sorten, numexplode=2
  Aehnlich,		# 2^omega Sorten
  Trees2,		# numexplode=0
  GoII			# numexplode=0

level[nofx]=
  Nasenkugeln,Embroidery,Octopi,Springer,Kacheln_Sechseck,
  Tiere,Baender,HexKugeln,Bonus,
  Jahreszeiten,Wohnungen,
  Kacheln_Viereck,Himmel,
  Labyrinth,Unmoeglich,Elemente,Hormone,
  Ebene

level[nofx,easy]=
  Nasenkugeln,Embroidery,Octopi,Springer,Kacheln_Sechseck,
  Tiere,Baender,HexKugeln,Bonus,
  Jahreszeiten,Wohnungen,
  Kacheln_Viereck,Himmel,
  Labyrinth,Unmoeglich,Elemente,Hormone,
  Ebene,Angst

level[weird]=
  Bonus,Fische,Pressure,Rechnen,
  Disco,Baggis,Tetris,
  Rollenspiel,Kolben,Baelle,Aliens,
  Slime,Flechtwerk,
  Explosive,Gold,Zahn,Doors,Bunt,
  Rohre,Darken,Dungeon,
  ColorShape,Jump,Memory,Schach,Aehnlich,ReversiBRL,
  Go,Tennis,
  Angst,Walls,Kacheln_azyklisch,Trees2,NoVIPs,Wachsen,GoII
  ,BoniMali2

level[weird,hard]=
  Bonus,Fische,Pressure,Rechnen,
  Disco,Baggis,Tetris,
  Rollenspiel,Kolben,Baelle,
  Slime,Flechtwerk,
  Explosive,Gold,Zahn,Doors,Bunt,
  Rohre,Darken,Dungeon,
  ColorShape,Jump,Memory,Schach,Aehnlich,ReversiBRL,
  Go,Tennis,
  Angst,Walls,Kacheln_azyklisch,Trees2,NoVIPs,Wachsen,GoII,BoniMali2,Secret

level[weird,easy]=
  Bonus,Fische,Pressure,Rechnen,
  Disco,Baggis,Tetris,
  Rollenspiel,Kolben,Baelle,
  Slime,Flechtwerk,
  Explosive,Gold,Zahn,Doors,Bunt,
  Rohre,Darken,Dungeon,
  ColorShape,Jump,Memory,Schach,Aehnlich,ReversiBRL,
  Go,Tennis,
  Walls,Kacheln_azyklisch,Trees2,NoVIPs,Wachsen,GoII,BoniMali2

level[contrib]=ASCII,BreakOut,Xtradick,mfs,ParatroopersInvers,Ziehlen,Pacman
ordered[contrib]=0

globals=globals.ld

end   # verbatim

